export const INITIAL_SLATE_VALUE = [
  {
    type: 'paragraph',
    children: [
      {
        text:
          '',
      },
    ],
  },
];

export const LIST_TYPES = ['numbered-list', 'bulleted-list'];

export const HOTKEYS = {
  'mod+b': 'bold',
  'mod+i': 'italic',
  'mod+u': 'underline',
  'mod+`': 'code',
};
