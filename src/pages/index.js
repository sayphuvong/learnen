import React from 'react';
import { Container, Box } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Head from 'next/head';
import ThemeContainer from '../components/molecule/ThemeContainer';

const useStyles = makeStyles((theme) => ({
  bodyWrap: {
    padding: theme.spacing(2),
  },
  avatar: {
    width: theme.spacing(21),
    height: theme.spacing(21),
  },
  welcomeText: {
    textAlign: 'center',
    fontSize: 70,
  },
}));

export default function Home() {
  const classes = useStyles();

  return (
    <ThemeContainer>
      <Head>
        <title>Welcome to Safu App</title>
      </Head>
      <Box className={classes.bodyWrap}>
        <h1 className={classes.welcomeText}>WELCOME TO SAFU APP</h1>
      </Box>
    </ThemeContainer>
  );
}
