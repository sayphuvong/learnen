import React, { useEffect } from 'react';
import { Box, CssBaseline, ThemeProvider } from '@material-ui/core';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { wrapper } from '../redux/store';
import Header from '../components/molecule/Header';
import { getCommon3000Words } from '../redux/common-3000-words/common-3000-words.action';
import MUITheme from '../styles/theme';
import { getCommon3000WordsFromFile } from '../utils/file';

import '../styles/normalize.css';
import '../styles/globals.css';
import '../styles/animate.css';

function App({ Component, pageProps }) {
  const theme = MUITheme('light');
  const dispatch = useDispatch();

  const saveCommon3000WordsData = (data) => {
    const validData = data || {};
    dispatch(getCommon3000Words(validData));
  };

  useEffect(() => {
    getCommon3000WordsFromFile(saveCommon3000WordsData);
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Box>
        <Header />
        <Component {...pageProps} />
      </Box>
    </ThemeProvider>
  );
}

App.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};

App.getInitialProps = async (appContext) => {
  const {
    Component,
    ctx: { req, store, res },
    ctx,
  } = appContext;

  const pageProps = {
    ...(Component.getInitialProps ? await Component.getInitialProps(ctx) : {}),
  };

  return {
    pageProps,
  };

  // dispatch(getCommon3000Words());
};

export default wrapper.withRedux(App);


