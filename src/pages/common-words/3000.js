import React, { useMemo, useEffect } from 'react';
import { makeStyles, Box } from '@material-ui/core';
import Head from 'next/head';
import { useDispatch, useSelector } from 'react-redux';
import loRandom from 'lodash/random';
import ThemeContainer from '~/components/molecule/ThemeContainer';
import FlashcardData from '~/components/molecule/FlashcardData';
import FlashcardControl from '~/components/molecule/FlashcardControl';
import { saveCurrentWord } from '~/redux/common-3000-words/common-3000-words.action';
import { isEmpty } from '~/utils/youDontNeedLodashUnderscore';

const useStyles = makeStyles((theme) => ({
  container: {
    color: theme.palette.primary.contrastText,
    paddingTop: theme.spacing(1),
  },
  containerContent: {
    // backgroundColor: 'deeppink',
    borderRadius: '10px',
    padding: theme.spacing(2, 0),
  },
  flashcardCustom: {
    marginTop: theme.spacing(4),
  },
}));

export default function Common3000Words() {
  const dispatch = useDispatch();
  const common3000Words = useSelector(({ common3000Words }) => common3000Words.dataFromFile);
  const currentWord = useSelector(({ common3000Words }) => common3000Words.currentWord);
  const classes = useStyles();

  useEffect(() => {
    console.log('!currentWord && common3000Words.length > 0', {
      currentWord,
      common3000Words
    })
    if (isEmpty(currentWord) && common3000Words.length > 0) {
      const randomIndex = loRandom(0, common3000Words.length - 1);
      
      dispatch(saveCurrentWord({
        index: randomIndex,
        data: common3000Words[randomIndex],
      }));
    }
  }, [common3000Words]);

  return (
    <ThemeContainer className={classes.container}>
      <Box className={classes.containerContent}>
        <Head>
          <title>Safu App ~ 3000 words common</title>
        </Head>
        <FlashcardControl />
        <FlashcardData extraClasses={classes.flashcardCustom} />
      </Box>
    </ThemeContainer>
  );
}
