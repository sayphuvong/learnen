import React from 'react';
import { Box, makeStyles } from '@material-ui/core';
import PropTypes from 'prop-types';

const useStyles = makeStyles((theme) => ({
  switchFieldWrap: {
    display: 'flex',
    width: '100%',
    padding: '0 1px',
    overflow: 'hidden',
    userSelect: 'none',
    '& > input': {
      position: 'absolute !important',
      clip: 'rect(0, 0, 0, 0)',
      width: 1,
      height: 1,
      border: 0,
      overflow: 'hidden',
    },
    '& > label': {
      flex: 1,
      backgroundColor: theme.palette.primary.contrastText,
      color: 'rgba(0, 0, 0, 0.6)',
      fontSize: '14px',
      lineHeight: 1,
      textAlign: 'center',
      padding: theme.spacing(1, 2),
      marginRight: -1,
      border: `1px solid ${theme.palette.secondary.contrastText}`,
      transition: 'all 0.1s ease-in-out',
    },
    '& > label:hover': {
      cursor: 'pointer',
    },
    '& > input:checked + label': {
      color: theme.palette.primary.contrastText,
      backgroundColor: theme.palette.success.main,
      boxShadow: 'none',
    },
    '& > label:first-of-type': {
      borderRadius: theme.spacing(0.5, 0, 0, 0.5),
    },
    '& > label:last-of-type': {
      borderRadius: theme.spacing(0, 0.5, 0.5, 0),
    },
  },
}));

export default function SwitchField(props) {
  const { options, initCheckedValue, groupName, onChange } = props;
  const initCheckedValueValid = initCheckedValue || options[0]?.value;
  const classes = useStyles();

  const handleInputOnChange = (event) => {
    const inputValue = event.target.value;
    if (onChange && typeof onChange === 'function') {
      onChange(inputValue);
    }
  };

  return (
    <form style={{ width: '100%' }}>
      <Box className={classes.switchFieldWrap}>
        {options.map(({ value, label, id }) => (
          <React.Fragment key={id}>
            <input
              type="radio"
              id={id}
              name={groupName}
              value={value}
              defaultChecked={value === initCheckedValueValid}
              onChange={handleInputOnChange}
            />
            <label htmlFor={id}>{label}</label>
          </React.Fragment>
        ))}
      </Box>
    </form>
  )
}

SwitchField.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.array,
        PropTypes.object,
      ]),
      label: PropTypes.string,
      id: PropTypes.string,
    })
  ),
  initCheckedValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
    PropTypes.array,
    PropTypes.object,
  ]),
  groupName: PropTypes.string.isRequired,
  onChange: PropTypes.func,
};

SwitchField.defaultProps = {
  options: [],
  initCheckedValue: null,
  onChange: null,
};
