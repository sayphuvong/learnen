import React, { useState, useMemo, useEffect } from 'react';
import { Box, makeStyles } from '@material-ui/core';
import clsx from 'clsx';
import loThrottle from 'lodash/throttle';

const TRANSITION_DURATION = 2000;

const useStyles = makeStyles((theme) => ({
  cardWrap: {
    width: "calc(100vw - 40px)",
    height: 400,
    minWidth: 280,
    maxWidth: 350,
    margin: theme.spacing(0, 'auto'),
    perspective: '600px',
    animation: 'cardAppear 1.4s forwards',
    cursor: 'pointer',
  },
  card: {
    width: '100%',
    height: '100%',
    position: 'relative',
    color: 'black',
    borderRadius: theme.spacing(1),
    transformStyle: 'preserve-3d',
    transform: 'rotateY(180deg)',
    cursor: 'pointer',
  },
  wrapper: {
    width: '100%',
    height: '100vh',
    paddingTop: theme.spacing(3),
  },
  cardFlipped: {
    animation: `cardFlip ${1000 / TRANSITION_DURATION}s forwards linear`,
  },
  cardUnFlipped: {
    animation: `cardUnFlip ${1000 / TRANSITION_DURATION}s forwards linear`,
  },
  cardFront: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 5,
    transform: 'rotateY(180deg)',
    boxShadow: `1px 1px 10px 1px ${theme.palette.secondary.contrastText}`,
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.primary.dark,
  },
  cardBack: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 4,
    boxShadow: `1px 1px 10px 1px ${theme.palette.secondary.contrastText}`,
    color: theme.palette.secondary.contrastText,
    backgroundColor: theme.palette.secondary.main,
  },
  cardFace: {
    position: 'absolute',
    backfaceVisibility: 'hidden',
    height: '100%',
    width: '100%',
    borderRadius: '9px',
    padding: theme.spacing(1, 2),
    fontSize: '1.2rem',
    overflow: 'auto',

    [theme.breakpoints.up("md")]: {
      fontSize: "1.5rem",
    },
    
    '& img': {
      width: '50%',
      margin: theme.spacing(0, 'auto'),
    },
  },
}));

export default function Flashcard({ frontEl, backEl, extraClasses }) {
  const [cardFlipped, setCardFlipped] = useState(false);
  const classes = useStyles();

  const setCardFlippedDelay = useMemo(() => {
    return loThrottle(setCardFlipped, 600);
  }, []);

  return (
    <Box component="section" className={clsx(extraClasses)}>
      <Box className={classes.wrapper}>
        <Box className={classes.cardWrap}>
          <Box
            className={clsx(classes.card, {
              [classes.cardFlipped]: cardFlipped,
              [classes.cardUnFlipped]: !cardFlipped,
            })}
            onClick={() => setCardFlippedDelay(!cardFlipped)}
          >
            {/* Card Back */}
            <Box className={clsx(classes.cardFace, classes.cardBack)}>
              {backEl}
            </Box>
            {/* Card Front */}
            <Box className={clsx(classes.cardFace, classes.cardFront)}>
              {frontEl}
            </Box>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
