import React from 'react';
import { Box, Typography, makeStyles } from '@material-ui/core';
import CancelPresentationIcon from '@material-ui/icons/CancelPresentation';

const useStyles = makeStyles((theme) => ({
  emptyWrapper: {
    padding: theme.spacing(2, 3),
  },
  content: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  emptyIcon: {
    fontSize: 75,
  },
}));

export default function Empty({ title }) {
  const classes = useStyles();

  return (
    <Box className={classes.emptyWrapper}>
      <Box className={classes.content}>
        <CancelPresentationIcon className={classes.emptyIcon} />
        <Typography>{title}</Typography>
      </Box>
    </Box>
  );
}
