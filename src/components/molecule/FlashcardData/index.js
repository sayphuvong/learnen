import React, { useMemo } from 'react';
import { Box, makeStyles } from '@material-ui/core';
import { useSelector } from 'react-redux';
import Flashcard from '~/components/atom/Flashcard';
import Empty from '~/components/atom/Empty';

const useStyles = makeStyles((theme) => ({
  cardBackContent: {
    display: 'flex',
    padding: theme.spacing(2, 0),
  },
  cardFrontContent: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: theme.typography.h3.fontSize,
  },
}));

export default function EnFlashcard(props) {
  const currentWord = useSelector(({ common3000Words }) => common3000Words.currentWord);
  const classes = useStyles();
  
  const frontElement = useMemo(() => {
    let element = null;
    if (!currentWord?.data?.word) {
      element = <Empty title="Empty" />;
    } else {
      element = <Box>{currentWord?.data?.word}</Box>;
    }
    return (
      <Box className={classes.cardFrontContent}>
        {element}
      </Box>
    );
  }, [currentWord]);

  const backElement = useMemo(() => {
    let element = null;
    if (!currentWord?.data?.description) {
      element = <Empty title="Empty" />;
    } else {
      element = <Box dangerouslySetInnerHTML={{ __html: currentWord?.data?.description }} />;
    }
    return (
      <Box className={classes.cardBackContent}>
        {element}
      </Box>
    );
  }, [currentWord]);
  
  return (
    <Box>
      <Flashcard
        frontEl={frontElement}
        backEl={backElement}
        {...props}
      />
    </Box>
  )
}
