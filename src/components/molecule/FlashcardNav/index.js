import React, { useState, useMemo, useEffect } from 'react';
import { Box, Typography, makeStyles } from '@material-ui/core';
import clsx from 'clsx';
import loThrottle from 'lodash/throttle';
import SettingsIcon from '@material-ui/icons/Settings';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';

const useStyles = makeStyles((theme) => ({
  container: {},
  content: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
    position: 'relative',
    color: theme.palette.text.primary,
  },
  settingIconWrap: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    right: 20,
    height: '100%',
  },
  settingIcon: {
    cursor: 'pointer',
    transition: 'all 0.2s ease-in',
    '&:hover': {
      transform: 'rotate(180deg)',
    },
    '&:active': {
      transform: 'rotate(180deg)',
    },
  },
  navWordControl: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    '& > svg:hover': {
      color: theme.palette.secondary.main,
      cursor: 'pointer',
      transition: 'all 0.2s ease-in',
    },
  },
  englishWord: {
    margin: theme.spacing(0, 3),
    minWidth: 150,
    textAlign: 'center',
    fontWeight: 700,
  }
}));

export default function FlashcardNav({ contentEl, leftAction, rightAction, settingAction }) {
  const classes = useStyles();

  const handleArrowBackClick = () => {
    if (typeof leftAction === 'function') {
      leftAction();
    }
  };

  const handleArrowForwardClick = () => {
    if (typeof rightAction === 'function') {
      rightAction();
    }
  };

  const handleSettingClick = () => {
    if (typeof settingAction === 'function') {
      settingAction();
    }
  };

  return (
    <Box id="flashcard-control" className={classes.container}>
      <Box className={classes.content}>
        <Box className={classes.navWordControl}>
          <ArrowBackIcon onClick={handleArrowBackClick} />
          <Typography className={classes.englishWord}>{contentEl}</Typography>
          <ArrowForwardIcon onClick={handleArrowForwardClick} />
        </Box>
        <Box className={classes.settingIconWrap}>
          <SettingsIcon className={classes.settingIcon} onClick={handleSettingClick} />
        </Box>
      </Box>
    </Box>
  );
}
