import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, Container } from '@material-ui/core';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  container: {
    [theme.breakpoints.up('lg')]: {
      maxWidth: '1140px',
    },
  },
}));

const ThemeContainer = ({ children, className, style, maxWidth = 'lg', ...rest }) => {
  const classes = useStyles();

  return (
    <Container
      style={style}
      maxWidth={maxWidth}
      className={clsx(classes.container, className)}
      {...rest}
    >
      {children}
    </Container>
  );
};

ThemeContainer.defaultProps = {
  children: undefined,
  className: '',
  maxWidth: undefined,
  style: {},
};

ThemeContainer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element, PropTypes.array,
    PropTypes.string
  ]),
  className: PropTypes.string,
  maxWidth: PropTypes.string,
  style: PropTypes.object,
};

export default ThemeContainer;
