import React from 'react';
import { Button as MuiButton, makeStyles } from '@material-ui/core';
import { useSlate } from 'slate-react';
import FormatBoldIcon from '@material-ui/icons/FormatBold';
import FormatItalicIcon from '@material-ui/icons/FormatItalic';
import FormatUnderlinedIcon from '@material-ui/icons/FormatUnderlined';
import CodeIcon from '@material-ui/icons/Code';
import LooksOneIcon from '@material-ui/icons/LooksOne';
import LooksTwoIcon from '@material-ui/icons/LooksTwo';
import FormatQuoteIcon from '@material-ui/icons/FormatQuote';
import FormatListNumberedIcon from '@material-ui/icons/FormatListNumbered';
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted';
import { isMarkActive, toggleMark, isBlockActive, toggleBlock } from '~/utils/slate.util';
import clsx from 'clsx';

const Button = ({ onMouseDown, children }) => {
  return (
    <MuiButton
      onMouseDown={onMouseDown}
    >
      {children}
    </MuiButton>
  );
};


const slateIconStyle = makeStyles((theme) => ({
  normal: {
    color: theme.palette.text.disabled,
  },
  active: {
    color: theme.palette.text.primary,
  }
}));

const SlateIcon = ({ icon, active }) => {
  const classes = slateIconStyle();
  let IconOutput = null;

  switch(icon) {
    case 'format_bold':
      IconOutput = FormatBoldIcon;
      break;
    case 'format_italic':
      IconOutput = FormatItalicIcon;
      break;
    case 'format_underlined':
      IconOutput = FormatUnderlinedIcon;
      break;
    case 'code':
      IconOutput = CodeIcon;
      break;
    case 'looks_one':
      IconOutput = LooksOneIcon;
      break;
    case 'looks_two':
      IconOutput = LooksTwoIcon;
      break;
    case 'format_quote':
      IconOutput = FormatQuoteIcon;
      break;
    case 'format_list_numbered':
      IconOutput = FormatListNumberedIcon;
      break;
    case 'format_list_bulleted':
      IconOutput = FormatListBulletedIcon;
      break;
  }

  return (
    <IconOutput
      className={clsx({
        [classes.normal]: !active,
        [classes.active]: active,
      })}
    />
  );
};

export const MarkButton = ({ format, icon }) => {
  const editor = useSlate();
  const active = isMarkActive(editor, format);
  console.log(`@@ active ${format}`, active)

  return (
    <Button
      onMouseDown={event => {
        event.preventDefault()
        console.log(`onMouseDown ${format}`)
        toggleMark(editor, format)
      }}
    >
      <SlateIcon icon={icon} active={active} />
    </Button>
  );
};

export const BlockButton = ({ format, icon }) => {
  const editor = useSlate();
  const active = isBlockActive(editor, format);
  console.log(`@@ active ${format}`, active)

  return (
    <Button
      onMouseDown={event => {
        event.preventDefault()
        toggleBlock(editor, format)
      }}
    >
      <SlateIcon icon={icon} active={active} />
    </Button>
  );
};
