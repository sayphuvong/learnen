import escapeHtml from 'escape-html';
import { Text } from 'slate';

const generateTextNode = (node) => {
  let pattern = '{{string}}';

  if (node.bold) {
    pattern = `<strong>${pattern}</strong>`;
  }

  if (node.code) {
    pattern = `<code>${pattern}</code>`;
  }

  if (node.italic) {
    pattern = `<em>${pattern}</em>`;
  }

  if (node.underline) {
    pattern = `<u>${pattern}</u>`;
  }

  return pattern;
};

export const serialize = node => {
  if (Text.isText(node)) {
    let string = escapeHtml(node.text);
    const stringPattern = generateTextNode(node);
    return stringPattern.replace('{{string}}', string);
  }

  const children = node.children.map(n => serialize(n)).join('');

  switch (node.type) {
    case 'block-quote':
      return `<blockquote><p>${children}</p></blockquote>`;
    case 'paragraph':
      return `<p>${children}</p>`;
    case 'link':
      return `<a href="${escapeHtml(node.url)}">${children}</a>`;
    case 'code':
      return `<code>${children}</code>`;
    case 'list-item':
      return `<li>${children}</li>`;
    case 'numbered-list':
      return `<ol>${children}</ol>`;
    case 'bulleted-list':
      return `<ul>${children}</ul>`;
    case 'heading-one':
      return `<h1>${children}</h1>`;
    case 'heading-two':
      return `<h2>${children}</h2>`;
    default:
      return children;
  }
};
