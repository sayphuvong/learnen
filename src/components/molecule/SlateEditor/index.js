// @refresh reset
import React, { useState, useMemo, useCallback, useEffect } from 'react';
import { Box, makeStyles } from '@material-ui/core';
import isHotkey from 'is-hotkey'
import { createEditor } from 'slate'
import {
  Slate,
  Editable,
  withReact,
} from 'slate-react'
import { withHistory } from 'slate-history'
import { HOTKEYS } from '~/constants/slate.const';
import { toggleMark } from '~/utils/slate.util';

import Element from './element';
import Leaf from './leaf';
import { MarkButton, BlockButton } from './components';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
  },
}));

export default function SettingContent({ initialValue, onChange }) {
  const [value, setValue] = useState(initialValue)

  const editor = useMemo(() => withHistory(withReact(createEditor())), [])
  const classes = useStyles();
  const renderElement = useCallback(props => <Element {...props} />, []);
  const renderLeaf = useCallback(props => <Leaf {...props} />, []);

  useEffect(() => {
    if (typeof onChange === 'function') {
      onChange(value);
    }
  }, [value]);

  return (
    <Box className={classes.root}>
      <Slate
        editor={editor}
        value={value}
        onChange={newValue => setValue(newValue)}
      >
        <div>
          <MarkButton format="bold" icon="format_bold" />
          <MarkButton format="italic" icon="format_italic" />
          <MarkButton format="underline" icon="format_underlined" />
          <MarkButton format="code" icon="code" />
          <BlockButton format="heading-one" icon="looks_one" />
          <BlockButton format="heading-two" icon="looks_two" />
          <BlockButton format="block-quote" icon="format_quote" />
          <BlockButton format="numbered-list" icon="format_list_numbered" />
          <BlockButton format="bulleted-list" icon="format_list_bulleted" />
        </div>
        <Editable
          renderElement={renderElement}
          renderLeaf={renderLeaf}
          placeholder="Enter some rich text…"
          spellCheck
          autoFocus
          onKeyDown={event => {
            for (const hotkey in HOTKEYS) {
              if (isHotkey(hotkey, event)) {
                event.preventDefault()
                const mark = HOTKEYS[hotkey]
                toggleMark(editor, mark)
              }
            }
          }}
        />
      </Slate>
    </Box>
  );
}
