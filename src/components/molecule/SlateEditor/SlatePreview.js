import React from 'react';
import { Box } from '@material-ui/core';
import { isSyncSerializeAndDeserialize } from '~/utils/slate.util';
import { serialize } from './serialize';

export default function SlatePreview({ slateValue }) {
  const slateValSerialized = slateValue.map(node => serialize(node));

  // Join '' to remove comma when rendering to html
  const html = slateValSerialized.join('');

  const HTMLContent = {
    dangerouslySetInnerHTML: {
      __html: html,
    }
  };

  // To testing
  // isSyncSerializeAndDeserialize(slateValue);
  
  return (
    <Box>
      <Box {...HTMLContent} />
    </Box>
  );
}
