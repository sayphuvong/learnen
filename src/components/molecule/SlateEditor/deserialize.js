
import { jsx } from 'slate-hyperscript';

export const deserialize = el => {
  if (el.nodeType === Node.TEXT_NODE) {
    return el.textContent
  } else if (el.nodeType !== Node.ELEMENT_NODE) {
    return null
  }

  let children = Array.from(el.childNodes).map(deserialize)

  if (children.length === 0) {
    children = [{ text: '' }]
  }

  switch (el.nodeName) {
    case 'BODY':
      return jsx('fragment', {}, children)
    case 'BR':
      return '\n'
    case 'BLOCKQUOTE':
      return jsx('element', { type: 'quote' }, children)
    case 'P':
      return jsx('element', { type: 'paragraph' }, children)
    case 'A':
      return jsx(
        'element',
        { type: 'link', url: el.getAttribute('href') },
        children
      )
    case 'OL':
      return jsx('element', { type: 'numbered-list' }, children)
    case 'LI':
      return jsx('element', { type: 'list-item' }, children)
    case 'UL':
      return jsx('element', { type: 'bulleted-list' }, children)
    case 'H1':
      return jsx('element', { type: 'heading-one' }, children)
    case 'H2':
      return jsx('element', { type: 'heading-two' }, children)
    case 'CODE':
      if (children.length === 1) {
        if (typeof children[0] === 'string') {
          return {
            code: true,
            text: children[0],
          };
        } else {
          return {
            code: true,
            ...children[0],
          };
        }
      }
    case 'STRONG':
      if (children.length === 1) {
        if (typeof children[0] === 'string') {
          return {
            bold: true,
            text: children[0],
          };
        } else {
          return {
            bold: true,
            ...children[0],
          };
        }
      }
    case 'U':
      if (children.length === 1) {
        if (typeof children[0] === 'string') {
          return {
            underline: true,
            text: children[0],
          };
        } else {
          return {
            underline: true,
            ...children[0],
          };
        }
      }
    case 'EM':
      if (children.length === 1) {
        if (typeof children[0] === 'string') {
          return {
            italic: true,
            text: children[0],
          };
        } else {
          return {
            italic: true,
            ...children[0],
          };
        }
      }
    default:
      return el.textContent;
  }
};
