import React, { useState, useEffect } from 'react';
import { Box, makeStyles } from '@material-ui/core';
import { useSelector, useDispatch } from 'react-redux';
import loThrottle from 'lodash/throttle';
import FlashcardNav from '~/components/molecule/FlashcardNav';
import { saveCurrentWord } from '~/redux/common-3000-words/common-3000-words.action';
import FlashcardSetting from './Setting';

const useStyles = makeStyles((theme) => ({
  flashcardWrapper: {},
}));

export default function FlashcardControl() {
  const dispatch = useDispatch();
  const [currWordState, setCurrWordState] = useState('');
  const [openSetting, setOpenSetting] = useState(false);
  const common3000Words = useSelector(({ common3000Words }) => common3000Words.dataFromFile);
  const currentWord = useSelector(({ common3000Words }) => common3000Words.currentWord);
  const classes = useStyles();

  const handNextWord = () => {
    const value = {
      index: currentWord.index + 1,
      data: common3000Words[currentWord.index + 1]
    };
    setCurrWordState(value.data.word);
    dispatch(saveCurrentWord(value));
  };

  const handlePreviousWord = () => {
    const value = {
      index: currentWord.index - 1,
      data: common3000Words[currentWord.index - 1]
    };
    setCurrWordState(value.data.word);
    dispatch(saveCurrentWord(value));
  };

  const handleFlashcardControlAction = loThrottle((control) => {
    if (control === 'left') {
      handlePreviousWord();
    } else if (control === 'right') {
      handNextWord();
    }
  }, 500);

  const handleFlashcardSettingAction = () => {
    setOpenSetting(!openSetting);
  };

  useEffect(() => {
    if (currentWord.data && currentWord.data.word) {
      setCurrWordState(currentWord.data.word);
    }
  }, [currentWord]);

  return (
    <Box className={classes.flashcardWrapper}>
      <FlashcardSetting open={openSetting} title="Flashcard Settings" onClose={() => setOpenSetting(false)} />
      <FlashcardNav
        contentEl={currWordState}
        leftAction={() => handleFlashcardControlAction('left')}
        rightAction={() => handleFlashcardControlAction('right')}
        settingAction={() => handleFlashcardSettingAction()}
      />
    </Box>
  );
}
