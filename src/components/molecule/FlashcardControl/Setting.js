import React, { useState, useEffect, useCallback } from 'react';
import {
  Box, makeStyles, Button, Dialog, DialogTitle,
  DialogContent as MuiDialogContent, DialogActions as MuiDialogActions, IconButton, Typography,
  useTheme,
} from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import SwitchField from '~/components/atom/SwitchField';
import SlateEditor from '~/components/molecule/SlateEditor';
import SlatePreview from '~/components/molecule/SlateEditor/SlatePreview';
import { FLASHCARD_SIDE } from '~/utils/constants';
import { INITIAL_SLATE_VALUE } from '~/constants/slate.const';
import { serialize } from '~/components/molecule/SlateEditor/serialize';
import { updateWordDescription, saveCommon3000Words } from '~/redux/common-3000-words/common-3000-words.action';

const useStyles = makeStyles((theme) => ({
  rootSettingContent: {
    height: '100%',
    display: 'flex',
  },
  dialogTitleRoot: {
    position: 'relative',
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    top: 0,
    right: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  paperStyle: {
    height: '80vh',
  },
  cardWrap: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    width: 350,
    height: '100%',
    marginRight: theme.spacing(2),
  },
  card: {
    flex: 1,
    width: '100%',
  },
  cardFrontSide: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 700,
    fontSize: '28px',
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.primary.dark,
    borderRadius: theme.spacing(0.5),
  },
  cardBackSide: {
    width: '100%',
    height: '100%',
    padding: theme.spacing(2),
    fontSize: '18px',
    borderRadius: theme.spacing(0.5),
    color: theme.palette.secondary.contrastText,
    backgroundColor: theme.palette.secondary.main,
  },
  settingContent: {
    flex: 1,
    backgroundColor: 'white',
  },
  btnSaveSetting: {
    backgroundColor: theme.palette.secondary.dark,
    color: theme.palette.primary.contrastText,
    "&:hover": {
      backgroundColor: theme.palette.secondary.dark,
      color: theme.palette.primary.contrastText,
    },
  },
}));

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    overflow: 'hidden',
    display: 'flex',
    justifyContent: 'center',
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(1),
  },
}))(MuiDialogActions);

export default function FlashcardSetting(props) {
  const { open, title, onClose } = props;
  const classes = useStyles();
  const dispatch = useDispatch();
  const [cardSide, setCardSide] = useState(FLASHCARD_SIDE.front);
  const [previewContent, setPreviewContent] = useState(INITIAL_SLATE_VALUE);
  const currentWord = useSelector(({ common3000Words }) => common3000Words.currentWord);

  const handleClose = () => {
    const slateValSerialized = previewContent.map(node => serialize(node));
    const html = slateValSerialized.join('');
    
    dispatch(updateWordDescription({
      word: currentWord?.data?.word,
      desc: html
    }));

    dispatch(saveCommon3000Words({
      index: currentWord?.index,
      word: currentWord?.data?.word,
      desc: html,
    }));

    if (typeof onClose === 'function') {
      onClose();
    }
  };

  const handleCloseButtonClick = () => {
    if (typeof onClose === 'function') {
      onClose();
    }
  };

  return (
    <Box>
      <Dialog
        maxWidth="lg"
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        classes={{
          paper: classes.paperStyle,
        }}
      >
        <DialogTitle disableTypography className={classes.dialogTitleRoot}>
          <Typography variant="h6">{title}</Typography>
          <IconButton aria-label="close" className={classes.closeButton} onClick={handleCloseButtonClick}>
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <Box width="90vw">
            <Box className={classes.rootSettingContent}>
              <Box className={classes.cardWrap}>
                <Box width="100%" marginBottom={2}>
                  <SwitchField
                    groupName="cardSide"
                    onChange={(val) => setCardSide(val)}
                    options={[
                      { label: 'Front Side', value: FLASHCARD_SIDE.front, id: FLASHCARD_SIDE.front },
                      { label: 'Back side', value: FLASHCARD_SIDE.back, id: FLASHCARD_SIDE.back },
                    ]}
                  />
                </Box>
                <Box className={classes.card}>
                  {cardSide === FLASHCARD_SIDE.front && (
                    <Box className={classes.cardFrontSide}>
                      {currentWord?.data?.word}
                    </Box>
                  )}
                  {cardSide === FLASHCARD_SIDE.back && (
                    <Box className={classes.cardBackSide}>
                      <SlatePreview slateValue={previewContent} />
                    </Box>
                  )}
                </Box>
              </Box>
              <Box className={classes.settingContent}>
                <SlateEditor initialValue={previewContent} onChange={setPreviewContent} />
              </Box>
            </Box>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} className={classes.btnSaveSetting}>
            Save changes
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}

FlashcardSetting.propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  onClose: PropTypes.func,
};

FlashcardSetting.defaultProps = {
  open: false,
  title: 'This place is title',
  onClose: f => f,
};
