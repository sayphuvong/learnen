import React from 'react';
import { Box, AppBar, Toolbar, IconButton, Typography, makeStyles } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import Link from 'next/link';

const useStyles = makeStyles((theme) => ({
  menuButton: {},
  rightContent: {
    marginLeft: 'auto',
    '& > a': {
      marginRight: theme.spacing(2),
      fontWeight: 700,
    },
    '& > a:hover': {
      color: theme.palette.secondary.dark,
      transition: 'all 0.2s ease-in',
    },
    '& > a:last-child': {
      marginRight: 0,
    },
  },
}));

export default function Header() {
  const classes = useStyles();

  return (
    <Box>
      <AppBar color="inherit" position="static">
        <Toolbar variant="dense">
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" color="inherit">
            SAFU APP
          </Typography>
          <Box className={classes.rightContent}>
            <Link href="/">Home</Link>
            <Link href="/common-words/3000">Common 3000 words</Link>
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
