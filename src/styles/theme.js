import { createMuiTheme } from '@material-ui/core/styles';

// Create a theme instance.
const theme = (colorTheme) => {
  return createMuiTheme({
    overrides: {
      MuiButton: {
        root: {
          borderRadius: '8px',
        },
      },
      MuiOutlinedInput: {
        input: {
          padding: '14px',
        },
      },
      MuiInputLabel: {
        formControl: {
          top: '-5px',
        },
      },
    },
    spacing: 8,
    palette: {
      type: colorTheme,
      text: {
        disabled: "#BDC3C7",
        primary: "#1E212D",
      },
      primary: {
        main: '#B8DFD8',
        dark: '#4C4C6D',
        light: '#FFE194',
        contrastText: '#E8F6EF',
      },
      secondary: {
        main: '#EABF9F',
        dark: '#B68973',
        light: '#FAF3E0',
        contrastText: '#1E212D',
      },
      error: {
        main: '#FF3B3B',
      },
      info: {
        main: '#0063F7',
      },
      success: {
        main: '#06C270',
      },
      warning: {
        main: '#FE9923',
      },
    },
    typography: {
      fontFamily: 'Noto Sans SC, sans-serif',
      h1: {
        fontWeight: 'bold',
        fontSize: '44px',
        lineHeight: '57.2px',
      },
      h2: {
        fontWeight: 'bold',
        fontSize: '36px',
        lineHeight: '46.8px',
      },
      h3: {
        fontWeight: 'bold',
        fontSize: '28px',
        lineHeight: '36.4px',
      },
      h4: {
        fontWeight: 'bold',
        fontSize: '24px',
        lineHeight: '31.2px',
      },
      h5: {
        fontWeight: 'bold',
        fontSize: '20px',
        lineHeight: '26px',
      },
      h6: {
        fontWeight: 'bold',
        fontSize: '16px',
        lineHeight: '20.8px',
      },
      body1: {
        fontSize: '16px',
        lineHeight: '27.2px',
      },
      body2: {
        fontSize: '14px',
        lineHeight: '23.8px',
      },
      subtitle1: {
        fontSize: '12px',
      },
    },
    breakpoints: {
      values: {
        xs: 0,
        sm: 600,
        md: 1024,
        lg: 1280,
        xl: 1920,
      },
    },
  });
};

export default theme;
