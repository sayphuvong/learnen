import { all } from 'redux-saga/effects';
import common3000WordsSaga from './common-3000-words/common-3000-words.saga';

export default function* rootSaga() {
  yield all([
    common3000WordsSaga(),
  ]);
}
