import { combineReducers } from 'redux';
import common3000Words from './common-3000-words/common-3000-words.reducer';

export default function rootReducer() {
  return combineReducers({
    common3000Words: common3000Words,
  });
}
