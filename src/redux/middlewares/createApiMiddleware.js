/**
 * API Middleware to handle api request side effects and provide consistent api response states
 * This middleware expect an action with the shape of:
 * ```
 * {
 *   types: [LOADING, SUCCESS, FAIL],
 *   promise: (request) => apiPromise,
 * };
 * ```
 * TODO: need unit test
 * @param {Object} client
 * @returns {Function} api middleware
 */

export default function createApiMiddleware() {
  return (store) => (next) => async (action) => {
    const { dispatch } = store;
    const { promise, types, callback, showNotiError, ...rest } = action;
    if (typeof promise !== 'function') {
      return next(action);
    }
    if (types.length !== 3) {
      throw new Error('API Middleware expect 3 action types: [REQUEST, SUCCESS, FAIL]');
    }

    const [REQUEST, SUCCESS, FAIL] = types;
    next({
      type: REQUEST,
      ...rest,
    });
    const actionPromise = promise();
    return actionPromise
      .then((result) => {
        next({
          result,
          type: SUCCESS,
          ...rest,
        });
        if (typeof callback === 'function') {
          callback();
        }
      })
      .catch((error) => {
        return next({
          error,

          type: FAIL,
          ...rest,
        });
      });
  };
}
