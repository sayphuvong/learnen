/* eslint-disable global-require */
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createWrapper } from 'next-redux-wrapper';
import createApiMiddleware from './middlewares/createApiMiddleware';
import rootReducer from './rootReducer';
import rootSaga from './rootSaga';
import configs from '../utils/configs';

const _initialState = {};

const composeEnhancers =
  (typeof window !== 'undefined' &&
    configs.env === 'development' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) ||
  compose;

const logger = store => next => action => {
  console.group(action.type)
  console.debug('dispatching', action)
  let result = next(action)
  console.debug('next state', store.getState())
  console.groupEnd(action.type)
  return result
}

const makeStore = () => {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [createApiMiddleware(), sagaMiddleware];
  const enhancer = composeEnhancers(applyMiddleware(logger, ...middlewares));
  const store = createStore(rootReducer(), _initialState, enhancer);

  store.sagaTask = sagaMiddleware.run(rootSaga);
  return store;
};

export { makeStore };
export const wrapper = createWrapper(makeStore);