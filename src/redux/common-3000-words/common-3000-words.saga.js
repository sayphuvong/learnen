import { call, put, takeLatest, select } from 'redux-saga/effects';
import {
  GET_COMMON_3000_WORDS,
  GET_COMMON_3000_WORDS_SUCCESS,
  SAVE_COMMON_3000_WORDS_TO_FILE,
} from './common-3000-words.action';
import { SAVE_COMMON_3000_WORDS } from '~/redux/api';
import { postData } from '~/utils/request';

function* getCommon3000Words({ data }) {
  yield put({ type: GET_COMMON_3000_WORDS_SUCCESS, data });
}

function* saveCommon3000WordsToFile({ index, word, desc }) {
  const common3000WordsState = yield select(({ common3000Words }) => common3000Words.dataFromFile);
  const originWord = common3000WordsState?.[index];
  console.log({ index, word, desc, originWord })
  if (originWord && originWord?.word === word) {
    console.log('hey')
    common3000WordsState[index] = {
      word,
      description: desc,
    };
    try {

      console.log('common3000WordsState', common3000WordsState)
      const response = yield call(postData, SAVE_COMMON_3000_WORDS, common3000WordsState);
      if (Array.isArray(response.data)) {
        yield put({ type: GET_COMMON_3000_WORDS_SUCCESS, data: response.data });
      }
      console.log('response', response);
    } catch (err) {
      console.log(err);
    }
  }
}

export default function* common3000WordsSaga() {
  yield takeLatest(GET_COMMON_3000_WORDS, getCommon3000Words);
  yield takeLatest(SAVE_COMMON_3000_WORDS_TO_FILE, saveCommon3000WordsToFile);
}
