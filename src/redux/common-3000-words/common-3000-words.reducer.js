import {
  GET_COMMON_3000_WORDS_SUCCESS,
  SAVE_CURRENT_WORD,
  UPDATE_CURRENT_WORD_DESCRIPTION,
} from './common-3000-words.action';

const initialState = {
  dataFromFile: [],
  currentWord: {},
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case GET_COMMON_3000_WORDS_SUCCESS: {
      return { ...state, dataFromFile: action.data };
    }

    case SAVE_CURRENT_WORD: {
      return { ...state, currentWord: action.data };
    }

    case UPDATE_CURRENT_WORD_DESCRIPTION: {
      const { index: currWordIndex } = state.currentWord;
      const { word, desc } = action;
      console.log({ word, desc })
      let newCurrentWord = {};
      if (state.currentWord?.index === currWordIndex && state.currentWord?.data?.word === word) {
        Object.assign(
          newCurrentWord,
          state.currentWord,
          {
            data: {
              word,
              description: desc
            }
          }
        );
      }

      return { ...state, currentWord: newCurrentWord };
    }

    default:
      return state;
  }
}
