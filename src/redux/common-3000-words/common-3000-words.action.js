export const SAVE_CURRENT_WORD = '@common-3000-words/SAVE_CURRENT_WORD';

export const GET_COMMON_3000_WORDS = '@common-3000-words/GET_COMMON_3000_WORDS';
export const GET_COMMON_3000_WORDS_SUCCESS = '@common-3000-words/GET_COMMON_3000_WORDS_SUCCESS';

export const UPDATE_CURRENT_WORD_DESCRIPTION = '@common-3000-words/UPDATE_CURRENT_WORD_DESCRIPTION';

export const SAVE_COMMON_3000_WORDS_TO_FILE = '@common-3000-words/SAVE_COMMON_3000_WORDS_TO_FILE';

export const getCommon3000Words = (data) => ({
  type: GET_COMMON_3000_WORDS,
  data,
});

export const saveCurrentWord = (data) => ({
  type: SAVE_CURRENT_WORD,
  data,
});

export const updateWordDescription = ({ word, desc }) => ({
  type: UPDATE_CURRENT_WORD_DESCRIPTION,
  word,
  desc,
});

export const saveCommon3000Words = ({ index, word, desc }) => ({
  type: SAVE_COMMON_3000_WORDS_TO_FILE,
  index,
  word,
  desc,
});
