const apiUrl = process.env.NEXT_PUBLIC_WEALTH_API_URL;
const baseApiUrl = process.env.NEXT_PUBLIC_WEALTH_BASE_API_URL;
const merchantCode = process.env.NEXT_PUBLIC_WEALTH_MERCHANT_CODE;
const dev = process.env.NODE_ENV !== 'production';
const staticPath = process.env.STATIC_PATH || '/';
const port = process.env.PORT || 3000;
const env = (typeof window !== 'undefined' ? window.ENV : process.env.NODE_ENV) || 'development';

export default {
  apiUrl,
  baseApiUrl,
  dev,
  merchantCode,
  env,
  staticPath,
  port,
};
