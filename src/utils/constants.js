const APP_ROUTE = {
  HOME: '/',
  COMMON_3000_WORDS: '/common-words/3000',
};

const FLASHCARD_SIDE = {
  front: 'front_side',
  back: 'back_side',
};

export {
  APP_ROUTE,
  FLASHCARD_SIDE,
};
