/* eslint-disable prefer-promise-reject-errors */
import axios from 'axios';
import configs from '~/utils/configs';
import { buildUrl, isServer } from './index';

const request = axios.create();
const defaultHeaders = {
  'Content-Type': 'application/json',
  merchantCode: configs.merchantCode,
};

request.defaults.baseURL = isServer ? (configs.baseApiUrl || 'http://localhost:3000') : '/';
request.defaults.timeout = 10000;

request.interceptors.response.use(
  (response) => response.data,
  (error) => {
    if (error.response) {
      const message = error.response.data?.message || error.response.data;
      return Promise.reject({
        code: error.response.status,
        message,
      });
    }
    return Promise.reject(error);
  },
);

const setToken = (token) => {
  defaultHeaders.cookie = token;
};

export { request, setToken };

export const setHeaderLanguage = (langId) => {
  if (langId) {
    defaultHeaders.languageId = langId;
  }
};

export const getData = (url, params = {}, headers = {}) => {
  const newUrl = buildUrl(url, params);
  return request.get(newUrl, {
    headers: Object.assign(defaultHeaders, headers),
  });
};

export const postData = (url, body = {}, params = {}, headers = {}) => {
  const newUrl = buildUrl(url, params);
  return request.post(newUrl, JSON.stringify(body), {
    headers: Object.assign(defaultHeaders, headers),
  });
};

export const putData = (url, body = {}, params = {}, headers = {}) => {
  const newUrl = buildUrl(url, params);
  return request.put(newUrl, JSON.stringify(body), {
    headers: Object.assign(defaultHeaders, headers),
  });
};

export const patchData = (url, body = {}, params = {}, headers = {}) => {
  const newUrl = buildUrl(url, params);
  return request.patch(newUrl, JSON.stringify(body), {
    headers: Object.assign(defaultHeaders, headers),
  });
};

export const deleteData = (url, params = {}, headers = {}) => {
  const newUrl = buildUrl(url, params);
  return request.delete(newUrl, {
    headers: Object.assign(defaultHeaders, headers),
  });
};
