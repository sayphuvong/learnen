import { stringify } from 'querystring';
import { CondOperator, RequestQueryBuilder } from '@nestjsx/crud-request';

export const isServer = typeof window === 'undefined';

const isValidObject = (value) => {
  if (!value) {
    return false;
  }
  const isArray = Array.isArray(value);
  const isBuffer = typeof Buffer !== 'undefined' && Buffer.isBuffer(value);
  const isObject = Object.prototype.toString.call(value) === '[object Object]';
  const hasKeys = !!Object.keys(value).length;
  return !isArray && !isBuffer && isObject && hasKeys;
};

const __spreadArrays = (
  (this && this.__spreadArrays) ||
  function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
      for (let a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) r[k] = a[j];
    return r;
  }
);

const flattenObject = (value, path) => {
  let _a;
  if (path === void 0) {
    path = [];
  }
  if (isValidObject(value)) {
    return Object.assign.apply(
      Object,
      __spreadArrays(
        [{}],
        Object.keys(value).map((key) => flattenObject(value[key], path.concat([key]))),
      ),
    );
  }
  return path.length ? ((_a = {}), (_a[path.join('.')] = value), _a) : value;
};

const composeQueryParams = (queryParams = {}) => stringify(flattenObject(queryParams));
const mergeEncodedQueries = (...encodedQueries) => {
  return (
    encodedQueries
      .map((query) => query)
      .filter((query) => query)
      .join('&')
  );
};

const prefixFilterDateRange = '**||between||**';

const composeFilter = (paramsFilter) => {
  const flatFilter = flattenObject(
    Object.keys(paramsFilter)
      .filter((key) => key !== '$or')
      .reduce(
        (obj, key) => ({
          ...obj,
          [key]: paramsFilter[key],
        }),
        {},
      ),
  );

  return Object.keys(flatFilter)
    .filter((key) => typeof flatFilter[key] !== 'object') // skip the empty-object filterValue when add new filter
    .map((key) => {
      let filterValue = flatFilter[key];

      const splitKey = key.split('||');
      let field = splitKey[0];
      let ops = splitKey[1];

      if (!ops) {
        if (['number', 'boolean'].includes(typeof filterValue) || filterValue.match(/^\d+$/)) {
          ops = CondOperator.EQUALS;
        } else if (filterValue.startsWith(prefixFilterDateRange)) {
          ops = CondOperator.BETWEEN;
          filterValue = filterValue.split(prefixFilterDateRange)[1];
        } else {
          ops = CondOperator.CONTAINS;
        }
      }

      if (field.startsWith('_') && field.includes('.')) {
        field = field.split(/\.(.+)/)[1];
      }
      return {
        field,
        operator: ops,
        value: filterValue,
      };
    });
};

export const buildUrl = (url, params = {}) => {
  const { filter = '{}', ...rest } = params;
  const encodedQueryParams = composeQueryParams({
    ...rest,
  });

  const encodedQueryFilter = RequestQueryBuilder.create({
    filter: composeFilter(JSON.parse(filter)),
  }).query();

  const query = mergeEncodedQueries(encodedQueryFilter, encodedQueryParams);

  return query ? `${url}?${query}` : url;
};
