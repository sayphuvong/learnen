import axios from 'axios';

export const getCommon3000WordsFromFile = async (callback) => {
  const { data: dataFromAxios = {} } = await axios.get('/common-3000-words.json');
  const { data: common3000WordsData } = dataFromAxios;
  if (typeof callback === 'function') {
    callback(common3000WordsData);
  }
};