import {
  Editor,
  Element as SlateElement,
  Transforms,
} from 'slate';
import { LIST_TYPES } from '~/constants/slate.const';
import { serialize } from '~/components/molecule/SlateEditor/serialize';
import { deserialize } from '~/components/molecule/SlateEditor/deserialize';

export const isMarkActive = (editor, format) => {
  const marks = Editor.marks(editor);
  console.log("@@ marks", {
    format,
    marks,
  });
  return marks ? marks[format] === true : false;
};

export const toggleMark = (editor, format) => {
  const isActive = isMarkActive(editor, format);
  console.log("@ toggleMark", {
    editor, format, isActive
  })

  if (isActive) {
    Editor.removeMark(editor, format);
  } else {
    Editor.addMark(editor, format, true);
  }
};

export const isBlockActive = (editor, format) => {
  const [ match ] = Editor.nodes(editor, {
    match: n => {
      console.log("@@ Check match", {
        n,
        'Editor.isEditor(n)': Editor.isEditor(n),
        'SlateElement.isElement(n)': SlateElement.isElement(n),
        'n.type': n.type,
      })
      return !Editor.isEditor(n) && SlateElement.isElement(n) && n.type === format;
    },
  });

  console.log("@@ match", {
    editor,
    format,
    match
  })
  
  return !!match;
};

export const toggleBlock = (editor, format) => {
  const isActive = isBlockActive(editor, format);
  const isList = LIST_TYPES.includes(format);

  Transforms.unwrapNodes(editor, {
    match: n =>
      LIST_TYPES.includes(
        !Editor.isEditor(n) && SlateElement.isElement(n) && n.type
      ),
    split: true,
  });
  const newProperties = {
    type: isActive ? 'paragraph' : isList ? 'list-item' : format,
  };
  Transforms.setNodes(editor, newProperties);

  if (!isActive && isList) {
    const block = { type: format, children: [] };
    Transforms.wrapNodes(editor, block);
  }
};

export const isSyncSerializeAndDeserialize = (slateValue) => {
  console.log('slateValue', slateValue);

  const slateValSerialized = slateValue.map(node => serialize(node));
  console.log('slateValSerialized', slateValSerialized);

  // Join '' to remove comma when rendering to html
  const html = slateValSerialized.join('');
  console.log('html', html);

  const document = new DOMParser().parseFromString(html, 'text/html');
  console.log('document.body', document.body);

  // Deserialize
  const result = deserialize(document.body);
  console.log('Deserialize', result);

  // Check
  const serializeAgain = result.map(node => serialize(node));
  const isComplete = serializeAgain.every((item, idx) => item === slateValSerialized[idx]);

  // Confirm the result
  if (isComplete) {
    console.log('COMPLETE!!!');
    return true;
  } else {
    return false;
  }
};
