const fs = require('fs');


const writeFile = (filePath, data) => {
  return new Promise((resolve, reject) => {
    try {
    const dataTemplate = JSON.stringify(data, null, 2);

    fs.writeFile(filePath, dataTemplate, (err) => {
      if (err) {
        reject(err);
      }

      console.log("Data is saved!");
      resolve({ fileContent: data});
    });
    } catch (err) {
      reject(err);
    }
  }); 
};

module.exports = { writeFile };
