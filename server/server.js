/* eslint-disable no-console */
/* eslint-disable no-new */
require('dotenv').config();
const compression = require('compression');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const express = require('express');
const next = require('next');
const path = require('path');
const _ = require('lodash');

const router = require('./router');

const dev = process.env.NODE_ENV !== 'production';
const staticPath = process.env.STATIC_PATH || '/';
const port = process.env.PORT || 3000;

const app = next({
  dev,
});

const handle = app.getRequestHandler();
app.prepare().then(() => {
  const server = express();
  app.setAssetPrefix(staticPath);

  server.use(express.static(path.join(__dirname, '../public')));

  server.use(compression());
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({ extended: false }));
  server.use(cookieParser());
  
  server.use('/api', router);
  server.use(handle);

  server.listen(3000, (err) => {
    if (err) {
      throw err;
    }
    console.log(`> Ready on http://localhost:${port}`);
  });
});
