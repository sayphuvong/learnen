const express = require('express');
const router = express.Router();
const path = require('path');

const jsonFile = require('./dto/jsonFile');

router.post('/save-common-3000-words', async (req, res) => {
  console.log('req.body', req.body);
  const arrWords = req.body;
  const common3000WordsPath = path.join(__dirname, '../public', '/common-3000-words.json');
  
  if (!Array.isArray(arrWords)) {
    res.status(422).json({});
  }
  
  const { fileContent } = await jsonFile.writeFile(common3000WordsPath, { data: arrWords });
  console.log('fileContent', fileContent)

  if (fileContent) {
    res.status(201).json({ data: fileContent.data });
  } else {
    res.status(500).json({})
  }
});

module.exports = router;
